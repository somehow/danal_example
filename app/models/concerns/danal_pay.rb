module DanalPay
	extend ActiveSupport::Concern

  ##########################################################
  # - CallTeledit
  #	다날 서버와 통신하는 함수입니다.
  #	debug가 true일경우 웹브라우저에 debugging 메시지를 출력합니다.
  ##########################################################
	def call_teledit(transR, teledit_bin_path, debug = false)
		bin = "SClient"

		arg = make_param( transR )

		input = "#{ teledit_bin_path }/#{ bin } \"#{ arg }\""

		output = `#{ input }`
		output.force_encoding("UTF-8")
		output = output.encode('UTF-8','EUC-KR')

		ret = $?.to_i

		if debug
			puts "Exec : #{ input.strip }"
			puts "Ret : #{ ret }"

			output.split("\n").each_with_index { |str, i| puts "Out Line[#{ i }]: #{ str.strip }" }
		end

		parsor( output )
	end

	def parsor(str, sep1 = "&", sep2 = "=")

		output = Hash.new
		input = ""

		if str.kind_of?(String)
			str.split("\n").each_with_index { |obj, i| input += "#{ obj }#{ sep1 }" }
		else 
			input = str
		end


		input.split(sep1).each_with_index do |obj, i|

			tmp = obj.split(sep2)

			name = tmp[0].strip
			value = tmp[1].strip

			tmp.each_with_index { |obj, j| value += "#{ sep2 }#{ obj.strip }" if j >= 2 }

			output[name] = value
		end

		output
	end

	def make_form_input (hash, arr=Array.new, prefix="")
		form_input_html = ""
		preLen = prefix.strip.length

		hash.keys.each do |key, isKeyIncludeArr = false|
			next if key.strip == "" || key[0, preLen] != prefix
			arr.each { |val| isKeyIncludeArr = true if key.include?(val) }

			form_input_html += "<input type=\"hidden\" name=\"#{ key }\" value=\"#{ hash[ key ] }\">" unless isKeyIncludeArr
		end

		form_input_html
	end

	def make_item_info(itemAmt, itemCode, itemName)
		itemInfo = "#{ itemCode[0, 1] }|#{ itemAmt }|1|#{ itemCode }|#{ itemName }"
	end

	def make_param(arr)
		ret = Array.new

		keys = arr.keys

		keys.each_with_index { |key, i| ret.push( "#{ key }=#{ arr[ key ] }") }

		make_info(ret)
	end

	def make_info(arr, joins = ";")
		ret = ""

		arr.each { |val| ret += "#{ val }" + joins }

		ret = ret[0...-1]
	end

	def get_item_name(cpName, ncpName,itemName,nitemName)
		"(#{ cpName[0, ncpName] }) #{ itemName[0, nitemName] }"
	end

	def get_ci_url(isUseCI,ciURL)

		# Default Danal CI
		url = "https://ui.teledit.com/Danal/Teledit/Web/images/customer_logo.gif"

		url = ciURL if isUseCI == "Y" && !ciURL.nil?

		url
	end

	def map2str(arr)
		ret = Array.new

		keys = arr.keys

		keys.each_with_index do |key, i|
			next unless key.strip
			ret << "#{key} = #{ arr[key] }"
		end

		ret.join("<BR>")
	end

	def get_bg_color(bgColor)
		# Default : Blue
		color = 0

		color = bgColor if 0 < bgColor.to_i && bgColor.to_i < 11

		sprintf("%02d",color)
	end

	# Ready용 transR 해쉬를 만든다.
	def init_transR(id, pwd, itemAmt, itemCode, itemName)
		###[ 필수 데이터 ]####################################
		transR = Hash.new

		######################################################
		## 아래의 데이터는 고정값입니다.( 변경하지 마세요 )
		# Command      : ITEMSEND2
		# SERVICE      : TELEDIT
		# ItemType     : Amount
		# ItemCount    : 1
		# OUTPUTOPTION : DEFAULT
		######################################################

		transR["Command"] = "ITEMSEND2"
		transR["SERVICE"] = "TELEDIT"
		transR["ItemType"] = "Amount"
		transR["ItemCount"] = "1"
		transR["OUTPUTOPTION"] = "DEFAULT"

		######################################################
		#  ID          : 다날에서 제공해 드린 ID
		#  PWD         : 다날에서 제공해 드린 PWD
		#  CPNAME      : CP 명
		######################################################
		transR["ID"] = id
		transR["PWD"] = pwd

		######################################################
		# ItemAmt      : 결제 금액
		#      - 실제 상품금액 처리시에는 Session 또는 DB를 이용하여 처리해 주십시오.
		#      - 금액 처리 시 금액변조의 위험이 있습니다.
		# ItemName     : 상품명
		# ItemCode     : 다날에서 제공해 드린 ItemCode
		######################################################

		transR["ItemInfo"] = make_item_info(itemAmt, itemCode, itemName)

		###[ 선택 사항 ]########################################
		######################################################
		# SUBCP        : 다날에서 제공하는 SUBCP ID
		# USERID       : 사용자 ID
		# ORDERID      : CP 주문번호
		# IsPreOtbill  : 자동결제 여부(Y/N) AuthKey 수신 유무 (자동결제를 위한 AuthKey 수신이 필요한 경우 : Y)
		# IsSubscript	: 월 정액 가입 유무(Y/N) (월 정액 가입을 위한 첫 결제인 경우 : Y)
		######################################################
		transR["SUBCP"] = "SUBCP"
		transR["USERID"] = "USERID"
		transR["ORDERID"] = "ORDERID"
		transR["IsPreOtbill"] = "N"
		transR["IsSubscript"] = "N"

		transR
	end

	# CPCGI의 confirm용 transR 해쉬를 만든다.
	def confirm_transR( serverInfo, nConfirmOption )
		transR = Hash.new
		transR["Command"] = "NCONFIRM"
		transR["OUTPUTOPTION"] = "DEFAULT"
		transR["ServerInfo"] = serverInfo
		transR["IFVERSION"] = "V1.1.2"
		transR["ConfirmOption"] = nConfirmOption

		transR
	end

	# CPCGI의 bill용 transR 해쉬를 만든다.
	def bill_transR( serverInfo, nBillOption )
		transR = Hash.new

		nBillOption = 0
		transR["Command"] = "NBILL"
		transR["OUTPUTOPTION"] = "DEFAULT"
		transR["ServerInfo"] = serverInfo
		transR["IFVERSION"] = "V1.1.2"
		transR["BillOption"] = nBillOption

		transR
	end

	# ByPassValue 해쉬를 만든다
	def init_by_pass_value(id)
		###[ 필수 데이터 ]####################################
		byPassValue = Hash.new

		byPassValue["BgColor"] = "00"
		byPassValue["TargetURL"] = url_for controller: 'pay', action: 'cpcgi', id: id
		byPassValue["BackURL"] = url_for controller: 'items', action: 'show', id: id
		byPassValue["IsUseCI"] = "N"
		byPassValue["CIURL"] = nil

		###[ 선택 사항 ]####################################

		#######################################################
		# Email	: 사용자 E-mail 주소 - 결제 화면에 표기
		# IsCharSet	: CP의 Webserver Character set
		#######################################################

		byPassValue["Email"] = "";
		byPassValue["IsCharSet"] = "UTF-8";

		#######################################################
		# CPCGI에 POST DATA로 전달 됩니다.
		#######################################################
		byPassValue["ByBuffer"] = "This value bypass to CPCGI Page"
		byPassValue["ByAnyName"] = "AnyValue"

		byPassValue
	end

	# teledit bin path를 정의한다.
	def init_teledit_bin_path (default, staging, production)

		teledit_bin_path = default
		teledit_bin_path = staging if Rails.env.staging?
		teledit_bin_path = production if Rails.env.production?

		raise "teledit_bin does not exist" unless File.exists? teledit_bin_path

		teledit_bin_path
	end

	def set_cache_buster
		response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
		response.headers["Pragma"] = "no-cache"
		response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"
	end

end
