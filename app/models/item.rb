class Item < ActiveRecord::Base
	validates :name, presence: true
	validates :amount, presence: true, numericality: { greater_than: 300, only_integer: true }
end
