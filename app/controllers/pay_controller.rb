class PayController < ApplicationController
	include DanalPay

	layout "danal"
	before_filter :set_cache_buster
	protect_from_forgery except: [:cpcgi]

	def ready

		@itemName = "상품이름-#{ params[:id] }"
		@itemAmt = Item.find_by_id( params[:id] ).amount
		@cpName = "CP이름"

		# transR 해시를 생성
		secret = Rails.application.secrets
		transR = init_transR( secret.danal_id, secret.danal_pwd, @itemAmt, secret.danal_item_code, @itemName)

		# ByPassValue 해시를 생성
		byPassValue = init_by_pass_value(params[:id])

		# teledit_bin_path 초기화
		danal_cp = Rails.configuration.x.danal_cp
		teledit_bin_path = init_teledit_bin_path( danal_cp.test, danal_cp.staging, danal_cp.production )

		#CP 검증 단계
		res = call_teledit( transR, teledit_bin_path, false )
		@res = res

		#검증 실패 시 Error 화면 구성을 위한 인스턴스 변수 생성 및 ready 액션 종료
		return set_error_params(byPassValue) unless res["Result"] == "0"

		#검증 성공 시 다날로 전송할 화면 구성을 위한 인스턴스 변수 생성
		@form_transR = make_form_input( res, ["Result", "ErrMsg"] )
		@form_by_pass_value = make_form_input( byPassValue )
		@IsPreOtbill = transR['IsPreOtbill']
		@IsSubscript = transR['IsSubscript']
	end

	def cpcgi

		itemId = params[:ItemName].split("-").last
		itemAmt = Item.find_by_id( itemId ).amount
		serverInfo = params[:ServerInfo]

		#################################################################
		# 결제 인증 default 옵션
		nConfirmOption = 1

		# transR 해시를 생성
		transR = confirm_transR( serverInfo, nConfirmOption )

		if nConfirmOption == 1
			transR["CPID"] = Rails.application.secrets.danal_id
			transR["AMOUNT"] = itemAmt
		end

		# ByPassValue 해시를 생성
		byPassValue = init_by_pass_value(itemId)

		# teledit_bin_path 초기화
		danal_cp = Rails.configuration.x.danal_cp
		teledit_bin_path = init_teledit_bin_path( danal_cp.test, danal_cp.staging, danal_cp.production )

		# 결제 인증 단계
		res = call_teledit( transR, teledit_bin_path, false )
		@res = res

		# 결제 인증 실패 시 Error 화면 구성을 위한 인스턴스 변수 생성 및 cpcgi 액션 종료
		return set_error_params(byPassValue) unless res["Result"] == "0"

		#################################################################
		# 결제 완료 defalut 옵션
		nBillOption = 0;

		# transR 해시를 생성
		transR = bill_transR( serverInfo, nBillOption )

		# 결제 완료 단계
		res2 = call_teledit( transR, teledit_bin_path, false )
		@res = res2

		# 결제 완료 실패 시 Error 화면 구성을 위한 인스턴스 변수 생성 및 cpcgi 액션 종료
		return set_error_params(byPassValue) unless res2["Result"] == "0"

		@form_http_var = make_form_input(params)
		@form_res = make_form_input( res, ["Result", "ErrMsg"] )
		@form_res2 = make_form_input( res2, ["Result", "ErrMsg"] )
	end

	def success
		# Success 화면 구성을 위한 인스턴스 변수 생성
		@BgColor	= get_bg_color(params[:BgColor])
		@URL = get_ci_url(params[:IsUseCI], params[:CIURL])
	end

	private
		def set_error_params(byPassValue)
			@AbleBack	= false
			@BgColor	= get_bg_color( byPassValue[ "BgColor" ] )
			@URL = get_ci_url( byPassValue[ "IsUseCI" ], byPassValue[ "CIURL" ] )
			@byPassValue = byPassValue
		end
end
